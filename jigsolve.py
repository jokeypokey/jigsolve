import glob
import cv2
import numpy as np
import itertools

# Jigsaw piece matcher
#
# This code will take a set of jigsaw pieces with black background as input and try  to find the best possible matches
# Read through the report for an overview on the process. Many aspects could be improved - particularly performance. If
# you have a weaker PC try improving the findBestFour function, as it is an o(n!) monstrosity. Overall the code is in
# dire need of some refactoring, so if you're looking for a great way to learn what the code does...
# If you have any questions about this code you can email me at jackalancraig@gmail.com
# But I would prefer to communicate through discord: jokeypokey#4051

cv2.destroyAllWindows()

# Load images
fileNames = glob.glob("resources/scans/*.jpg")
fileNames.sort()

print("opening file")
print(fileNames[0])
print(fileNames[1])
test_image1 = cv2.imread(fileNames[0])
test_image2 = cv2.imread(fileNames[1])
test_images = [test_image1, test_image2]


cv2.namedWindow('main', cv2.WINDOW_NORMAL)
cv2.resizeWindow('main', 1280, 720)

# Determines if the contour is concave or convex depending on the contour itself and the position of the center
# A point halfway between the two corners of the edge is taken along with the mmean point of the contour, the dot
# product of two vectors between these 3 points is taken as whether the contour is convex. Does not check for flat edges
def isContourConvex(contours, centre):
    result = []
    for contour in contours:
        mean_point = np.mean(contour, axis = 0)[0]
        halfway_point = ((contour[0] + contour[-1]) / 2)[0]
        v1 = halfway_point - mean_point
        v2 = halfway_point - centre
        dot = np.dot(v1, v2)
        result.append(dot < 0)
    return result

# Finds the absolute angle between two vectors
def absAngleBetweenVectors(v1, v2):
    v1_size = (v1[0] ** 2 + v1[1] ** 2) ** 0.5
    v2_size = (v2[0] ** 2 + v2[1] ** 2) ** 0.5
    v1_scaled = (v1[0] / v1_size, v1[1] / v1_size)
    v2_scaled = (v2[0] / v2_size, v2[1] / v2_size)
    return abs(np.cross(v1_scaled, v2_scaled))

# heuristic to find the 'rectangularness' of a set of four points
def rectangularness_finder(combination):
    # THe combination of points is not guaranteed to be an outward facing rectangle so four possibl orderings are needed
    # to guarantee that one is
    orderings = [(combination[0], combination[1], combination[2], combination[3]),
                 (combination[1], combination[0], combination[2], combination[3]),
                 (combination[2], combination[1], combination[0], combination[3]),
                 (combination[3], combination[1], combination[2], combination[0])]
    contours = [np.array(order) for order in orderings]
    areas = [abs(cv2.contourArea(contour)) for contour in contours]
    max_index = areas.index(max(areas))
    area = areas[max_index]
    contour = contours[max_index]

    interior_angle = absAngleBetweenVectors(contour[0] - contour[2], contour[1] - contour[3])
    corner1 = absAngleBetweenVectors(contour[0] - contour[1], contour[1] - contour[2])
    corner2 = absAngleBetweenVectors(contour[1] - contour[2], contour[2] - contour[3])
    corner3 = absAngleBetweenVectors(contour[2] - contour[3], contour[3] - contour[0])
    corner4 = absAngleBetweenVectors(contour[3] - contour[0], contour[0] - contour[1])

    # the minimum angle between the two vectors
    if min(corner1, corner2, corner3, corner4) < 0.98:
        return 0
    if interior_angle > 0.95:
        return area
    return 0


# Finds the best four corners from all possible corners
# Tries to maximise squareness and size
def findBestFour(corners):
    max_val = -9999
    max_comb = None
    for combination in itertools.combinations(corners, 4):
        val = rectangularness_finder(combination)
        if val > max_val:
            max_val = val
            max_comb = combination
    return np.array(max_comb)


# Finds the closet points along the contour to the given points
# Returned values will be from the contour array
def findIndexesOfClosestPointsToCorners(hull, points):
    output = []
    for corner in points:
        min_dist = 999999999
        closest_index = 0
        for pointIndex in range(len(hull)):

            closest = hull[0][0]
            actual_point = hull[pointIndex][0]
            distance = ((corner[0] - actual_point[0]) ** 2) + ((corner[1] - actual_point[1]) ** 2) ** 0.5
            if distance < min_dist:
                min_dist = distance
                closest_index = pointIndex
        output.append(closest_index)
    return sorted(output)


# This function does the entire pipeline
def refreshImage(x):
    binary_thresh = 15
    kernel_size = 5
    min_piece_size = 1000

    # Set this to 1 to test other image
    img_index = 0
    img = test_images[img_index].copy()

    if img_index == 0:
        img_scaled = cv2.resize(img, (2460, 1632))
    elif img_index == 1:
        img_scaled = cv2.resize(img, (2460, 1843))

    # Binary Threshholding on the image to isolate black background
    img_gray = cv2.cvtColor(img_scaled, cv2.COLOR_BGR2GRAY)
    img_gray_blurred = cv2.GaussianBlur(img_gray, (5, 5), 0)
    ret, img_thresh = cv2.threshold(img_gray_blurred, binary_thresh, 255, cv2.THRESH_BINARY)

    # Morpohological closing to remove holes
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    img_morphed = cv2.morphologyEx(img_thresh, cv2.MORPH_CLOSE, kernel)

    # Find all connected componenents, those of large enough size are pieces
    num_comp, output, stats, centroids = cv2.connectedComponentsWithStats(img_morphed)

    x_size = output.shape[0]
    y_size = output.shape[1]
    border = 15

    pieces = []
    piece_sides = []
    sides_concave = []
    sides_convex = []
    sides_flat = []

    # Create a canvas for XOR edge matching, must make the canvas large enough to fit all pieces
    xor_canvas_length = 0

    # Create cropped images for all pieces
    for i in range(1, num_comp):
        pts = np.where(output == i)
        if len(pts[0]) < min_piece_size:
            # remove components smaller than mimimum piece size
            output[pts] = 0
        else:
            # Locate edges of image to be cropped with a small border
            minx = max(0, np.min(pts[0])- border)
            maxx = min(x_size, np.max(pts[0]) + border)
            miny = max(0, np.min(pts[1]) - border)
            maxy = min(y_size, np.max(pts[1]) + border)

            # Create cropped image
            piece_binary = output[minx:maxx, miny:maxy]

            # enlarge xor canvas if needed
            biggest = max(piece_binary.shape)
            if biggest > xor_canvas_length:
                xor_canvas_length = biggest

            piece_binary[piece_binary == i] = 1
            piece_binary[piece_binary > 1] = 0

            moved_centroid = (centroids[i][1] - minx, centroids[i][0] - miny,)

            # Create tuple to contain all piece information - recommend changing this to a named tuple BTW
            pieces.append((len(pts[0]), img_scaled[minx:maxx, miny:maxy], piece_binary, moved_centroid))

    # Process each piece
    for i in range(len(pieces)):
        piece = pieces[i][1]
        piece_blurred_for_colour_swab = cv2.blur(piece, (25, 25,))
        piece_binary = pieces[i][2]
        centre = pieces[i][3]

        piece_grey = cv2.cvtColor(piece, cv2.COLOR_BGR2GRAY)
        piece_binary_grey = piece_binary.astype(np.uint8)
        piece_binary_grey_blurred = cv2.blur(piece_binary_grey, (5, 5,))
        piece_binary_grey[x == 1] = 255

        # Find contours of piece shape
        contours, heirachy = cv2.findContours(piece_binary_grey, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        main_contour = contours[0]

        # Find Harris corners of piece
        corners = cv2.cornerHarris(piece_binary_grey_blurred, 10, 3, 0.04)
        corners = cv2.dilate(corners, None)
        ret, corners = cv2.threshold(corners, 0.2 * corners.max(), 255, 0)
        corners = np.uint8(corners)

        # Refine corner selection to exact points
        ret, corner_labels, corner_stats, corner_centroids = cv2.connectedComponentsWithStats(corners)
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
        corners_accurate = cv2.cornerSubPix(piece_grey, np.float32(corner_centroids), (5, 5), (-1, -1), criteria)[1:]
        four_corners_accurate = findBestFour(corners_accurate)
        four_corner_drawable = np.int0(four_corners_accurate)

        # Draw dots on the four corners - but in a really dumb way...
        for corner in four_corner_drawable:
            piece[corner[1]-1][corner[0]-1]= np.array([255,255,0])
            piece[corner[1]-1][corner[0]] = np.array([255, 255, 0])
            piece[corner[1]-1][corner[0]+1] = np.array([255, 255, 0])
            piece[corner[1]][corner[0]-1] = np.array([255, 255, 0])
            piece[corner[1]][corner[0]] = np.array([255, 255, 0])
            piece[corner[1]][corner[0]+1] = np.array([255, 255, 0])
            piece[corner[1]+1][corner[0]-1] = np.array([255, 255, 0])
            piece[corner[1]+1][corner[0]] = np.array([255, 255, 0])
            piece[corner[1]+1][corner[0]+1] = np.array([255, 255, 0])

        corner_indexes = findIndexesOfClosestPointsToCorners(main_contour, four_corners_accurate)

        # Separate four edges of the piece by from the corner information
        contour1 = main_contour[corner_indexes[0]:corner_indexes[1] + 1]
        contour2 = main_contour[corner_indexes[1]:corner_indexes[2] + 1]
        contour3 = main_contour[corner_indexes[2]:corner_indexes[3] + 1]
        contour4 = np.concatenate((main_contour[corner_indexes[3]:],  main_contour[:corner_indexes[0] + 1]), axis=0)
        contour_split = [contour1, contour2, contour3, contour4]

        halfway_centre = np.mean(four_corners_accurate, axis=0)
        is_out_facing = isContourConvex(contour_split, halfway_centre)

        # Did some experimentation polar coordinates. Leaving this code here if you want to use it
        # main_contour_but_polar = np.empty_like(main_contour)
        # main_contour_but_polar = main_contour_but_polar.astype(float)
        #
        # main_contour_but_polar[:, :, 0] = ((main_contour[:, :, 0] - centre[0]) ** 2 + (main_contour[:, :, 1] - centre[1]) ** 2)
        # main_contour_but_polar[:, :, 1] = np.arctan2(main_contour[:, :, 0] - centre[0], main_contour[:, :, 1] - centre[1])

        # Process each edge
        for contour in range(len(contour_split)):
            current_contour = contour_split[contour]
            start = current_contour[0][0]
            end = current_contour[-1][0]
            mid = (start + end) / 2
            diff = mid - start
            startToEnd = end-start

            corner_angle = np.arctan2(startToEnd[0], startToEnd[1])

            # A triangle shape is generated to facilitate shape and XOR matching. The right corner_angle point of this triangle
            # can be interior or exterior depending on what is needed.
            option1 = mid + (diff[1], -diff[0])
            option2 = mid + (-diff[1], diff[0])
            distance_to_centre1 = (option1[0] - halfway_centre[0]) ** 2 + (option1[1] - halfway_centre[1]) ** 2
            distance_to_centre2 = (option2[0] - halfway_centre[0]) ** 2 + (option2[1] - halfway_centre[1]) ** 2
            if distance_to_centre1 < distance_to_centre2:
                triangle_point_interior = option1
                triangle_point_exterior = option2
            else:
                triangle_point_interior = option2
                triangle_point_exterior = option1

            # Take samples of the hue at various points
            size = len(contour_split[contour])
            hue_sample_count = 10
            hue_samples = []
            for s in range(hue_sample_count):
                index = int(((s+1) * size) / (hue_sample_count + 2))
                sample_location = contour_split[contour][index][0]
                colour_sample = piece_blurred_for_colour_swab[sample_location[1]][sample_location[0]]

                # Needs to be as an image so openCV can convert it to hsv.
                colour_sample_image = np.array([[colour_sample]])
                hsv_colour = cv2.cvtColor(colour_sample_image, cv2.COLOR_BGR2HSV)
                hue_samples.append(hsv_colour[0][0][0])
            hue_samples = np.array(hue_samples)

            # Create a canvas to place the edge shape so that it can be compared for XOR
            canvas = np.zeros((xor_canvas_length, xor_canvas_length, 3), dtype=np.uint8)
            centre = np.array([xor_canvas_length//2, xor_canvas_length//2])
            edge_to_centre_translation = (centre-mid).astype(np.intc)

            if abs(cv2.contourArea(contour_split[contour])) < 200:
                # The edge is flat
                modified_contour = current_contour.copy()
                piece_sides.append((i, contour, 0, modified_contour, hue_samples))
                sides_flat.append((i, contour, 0, modified_contour, hue_samples))
                colour = (0, 255, 255)
            elif is_out_facing[contour]:
                # The edge is convex
                modified_contour = np.append(current_contour, np.array([[triangle_point_interior]]), axis=0).astype(np.intc)
                far_contour = np.append(current_contour, np.array([[triangle_point_exterior]]), axis=0).astype(np.intc)

                far_contour_translated = far_contour[:, :, :] + edge_to_centre_translation
                cv2.drawContours(canvas, [far_contour_translated], 0, (255,255,255), cv2.FILLED)
                rot_mat = cv2.getRotationMatrix2D(tuple(centre), (-corner_angle*180)/np.pi - 45, 1.0)
                rotated_canvas = cv2.warpAffine(canvas, rot_mat, (xor_canvas_length, xor_canvas_length), flags=cv2.INTER_LINEAR)

                colour = (0, 255, 0)
                sides_convex.append((i, contour, 1, modified_contour, hue_samples, rotated_canvas))
                piece_sides.append((i, contour, 1, modified_contour, hue_samples, rotated_canvas))
            else:
                # The edge is concave
                modified_contour = np.append(current_contour, np.array([[triangle_point_exterior]]), axis=0).astype(np.intc)
                far_contour = np.append(current_contour, np.array([[triangle_point_exterior]]), axis=0).astype(np.intc)
                far_contour_translated = far_contour[:,:,:] + edge_to_centre_translation
                # We must reverse the contour since opencv defines shapes as counterclockwise(i think???) and this isn't
                reversed_contouur = modified_contour[::-1]

                cv2.drawContours(canvas, [far_contour_translated], 0, (255, 255, 255), cv2.FILLED)
                rot_mat = cv2.getRotationMatrix2D(tuple(centre), (-corner_angle * 180) / np.pi + 135, 1.0)
                rotated_canvas = cv2.warpAffine(canvas, rot_mat, (xor_canvas_length, xor_canvas_length),
                                               flags=cv2.INTER_LINEAR)

                colour = (0, 0, 255)
                piece_sides.append((i, contour, -1, reversed_contouur, hue_samples, rotated_canvas))
                sides_concave.append((i, contour, -1, reversed_contouur, hue_samples, rotated_canvas))

            cv2.drawContours(piece, [modified_contour], 0, colour, 1)

            piece = cv2.putText(piece, ['A', 'B', 'C', 'D'][contour], (int(mid[0]), int(mid[1])),
                                cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))

            # Display some XOR masks for your viewing pleasure
            if i < 5:
                cv2.namedWindow('piece #' + str(i) + " " + str(contour), cv2.WINDOW_NORMAL)
                cv2.imshow('piece #' + str(i) + " " + str(contour), rotated_canvas)

        if i < 99:
            piece = cv2.putText(piece, str(i), (int(halfway_centre[0]), int(halfway_centre[1])), cv2.FONT_HERSHEY_PLAIN,
                                2, (0, 255, 255))
            cv2.namedWindow('piece #' + str(i), cv2.WINDOW_NORMAL)
            cv2.imshow('piece #' + str(i), piece)

    best_convex_for_concaves = []
    all_matches_combined = []

    # Find all matches between convex and concave shapes
    for concave in sides_concave:
        best_convex = None
        best_val = 9999
        for convex in sides_convex:
            if convex[0] != concave[0]:
                #2 is mirrored bad???
                shape_heuristic = cv2.matchShapes(concave[3], convex[3], cv2.CONTOURS_MATCH_I3, 0.0)
                xor_image = np.bitwise_xor(concave[5], convex[5])
                trimmed_xor_image = xor_image[xor_canvas_length//3:2*xor_canvas_length//3, xor_canvas_length//3:2*xor_canvas_length//3]
                xor_heuristic = np.mean(xor_image)
                if convex[0] == 0:
                    cv2.namedWindow(f"xor {concave[0]}:{['A','B','C','D'][concave[1]]} + {convex[0]}:{['A','B','C','D'][convex[1]]}", cv2.WINDOW_NORMAL)
                    cv2.imshow(f"xor {concave[0]}:{['A','B','C','D'][concave[1]]} + {convex[0]}:{['A','B','C','D'][convex[1]]}", trimmed_xor_image)

                # opencv hue value caps at 180 and these are 8bit ints.
                # since the max diff is 90 this should be fine... right?
                diffs2 = convex[4] - concave[4]
                diffs1 = concave[4] - convex[4]

                diffs = np.minimum(diffs1, diffs2)
                hue_heuristic = sum([x**2 for x in diffs])

                overall_heuristic = shape_heuristic * 0.2 + (hue_heuristic * 0.0005) + xor_heuristic * -0.1
                all_matches_combined.append((overall_heuristic, concave, convex, shape_heuristic, hue_heuristic, xor_heuristic))
                if shape_heuristic < best_val:
                    best_val = shape_heuristic
                    best_convex = convex
        best_convex_for_concaves.append((concave, best_convex, best_val, hue_heuristic))

    used_matches = set()
    matches_counter = 0
    print_at_end = []

    # Goes through all matche to find matches that are bi-directionally best matches.
    for h, concave, convex, value, col, xor in sorted(all_matches_combined):
        ccString = str(concave[0]) + ['A','B','C','D'][concave[1]]
        cvString = str(convex[0]) + ['A', 'B', 'C', 'D'][convex[1]]
        print(f"Assessing between {concave[0]}:{['A','B','C','D'][concave[1]]} and {convex[0]}:{['A','B','C','D'][convex[1]]} with h value {h}, shape {value}, and colour {col} and xor {xor}")
        if not ccString in used_matches and not cvString in used_matches:
            print_at_end.append(f"Best between {concave[0]}:{['A','B','C','D'][concave[1]]} and {convex[0]}:{['A','B','C','D'][convex[1]]} with h value {h}, shape {value}, and colour {col} and xor {xor}")
            matches_counter += 1
        used_matches.add(ccString)
        used_matches.add(cvString)
    print(f"found {matches_counter} two way matches")

    for p in print_at_end:
        print(p)


    label_hue = np.uint8(35 * output / np.max(output))
    blank_ch = 255 * np.ones_like(label_hue)
    labeled_img = cv2.merge([label_hue, blank_ch, blank_ch])
    labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)
    labeled_img[label_hue == 0] = 0

    cv2.imshow('main', img_scaled)


refreshImage(0)

while not cv2.waitKey(1) & 0xFF == ord('q'):
    pass

cv2.destroyAllWindows()
