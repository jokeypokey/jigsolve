Jigsolve

Demo at https://www.youtube.com/watch?v=ybndcRmNFC8

Jigsaw puzzle image processing pipeline and piece match finder using openCV2. Designed to take an input jigsaw state and
suggest a list of best matches for each piece edge.

How to run:

Wing:
1. Open jigsolve.py in Wing IDE
2. Press the green play arrow.

Pycharm:
1. Open directory in PyCharm
2. Right click jigsolve.py and select run


Changing test data:

set the img_index variable in jigsolve.py to either 0 or 1 to test between data.


